## About
This simple repository adds [weno1d](https://bitbucket.org/fantaz/weno1d) 
repository from [bitbucket](https://bitbucket.org), and uses it in the application.

The central point is git's [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
It doesn't hurt to look at other references
[here](https://chrisjean.com/git-submodules-adding-using-removing-and-updating/),
or alternatively, SmartGit's [help page](https://www.syntevo.com/doc/display/SG/Submodules).

