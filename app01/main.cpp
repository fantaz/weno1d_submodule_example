#include "weno1d.h" // includes "barycentric_lagrange.h
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
    weno1d::rational5 wi;
    std::vector<double> y = { 0., 0., 0., 0., 0., 0.5, 0.5, 0.5, 0.5, 0.5 };
    // We're interested in interpolation of interval
    // [0 - 0.5] which is at position y[4]
    // Need to step back for R-1 points
    auto start_node = y.begin() + 4 - wi.order() + 1;
    std::cout << wi(0.8, start_node) << std::endl;

    return 0;
}
